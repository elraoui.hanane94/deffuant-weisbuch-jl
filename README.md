# deffuant-weisbuch-julia

This repo contains a .jl code to run simulations for a network-based Deffuant—Weisbuch opinion dynamics model. See [this blog post](https://unchitta.com/blog/2020/10/deffuant-weisbuch-julia) for a description of this model.

# How to use

The model parameters are
1. N : number of agents in the system
2. p : for generating a G(N,p) random graph
3. c : confidence bound
4. alpha : convergence parameter
5. q : how many nodes to update asynchronously at each time step

You can use `DW.jl` to initialze a model instance and use it to run a trial. For example:
```
include("DW.jl")

N = 100; p=0.05; c=0.3; alpha=0.1; q=1
model = initialize_model(N,p,c,alpha,q)
max_steps = 100000
data = run!(model,max_steps)
```
Then, you can  use the data to plot a time series of the opinions of the agents:
```
using Plots

steps = size(data)[1]
plot(1:steps, data, label=false)
```

It is a fun exercise to go through different parameter values and see what happens to the system. For example, `p=1` gives a complete graph and the original, basic DW model is recovered. When `c<0.2`, for example, we tend to get clusters of opinions at the end.

If you have questions or suggestions for improvements, please email ukanjana@gmu.edu.
